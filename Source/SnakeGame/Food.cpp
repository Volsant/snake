// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "SnakeBase.h"

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFood::SpawnFood()
{
	int x = -450 + rand() % 900;
	int y = -450 + rand() % 900;
	FVector FoodLoc (x, y, 0);
	FTransform FoodTransform(FoodLoc);
	AFood* NewFood = GetWorld()->SpawnActor<AFood>(FoodActorClass, FoodTransform);
}

void AFood::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->AddSnakeElement();
			SpawnFood();
			this->Destroy();
			
		}
		
	}
}



